# Kotlin Programming Patterns

This repository catalogs some patterns I have encountered and repeatedly used while writing Kotlin code.

## Patterns

- [Optional Template Procedure](/patterns/Optional%20Template%20Procedure.md)
- [Super Enums](/patterns/Super%20Enums.md), including Hierarchical Enum
