# Optional Template Procedure

A downside of virtual methods is that you must rely on the subclass's method to call the super method. This violates the Open/Closed principle, because the class is not "closed for modification".

One way to ensure the Open/Closed principle is followed is to use abstract classes with template methods:

```kotlin
abstract class Foo
{
    fun bar()
    {
        // do stuff here //
        barExtension()
    }

    abstract fun barExtension()
}
```

Now we can be sure that everything in `bar()` will be executed for every subclass of `Foo`, because the only method that sub-classes can override is `barExtension()`.

However, if `barExtension()` has no return value and the subclass doesn't need it to do anything special, then the subclass may end up with a load of empty/no-op "implementations" like this:

```kotlin
class FooImpl : Foo()
{
    // Without this line, a compilation error is thrown:
    override fun barExtension() = Unit
}
```

While the override is only one line long, having many such methods results in unnecessary bloat.

The solution is to mark `barExtension()` as *open* rather than abstract, and give it an empty body
in the abstract class:

```kotlin
abstract class FooRevised
{
    fun bar()
    {
        // do stuff here //
        barExtension()
    }

    // no-op unless overridden.
    open fun barExtension() = Unit
}
```

Subclasses are then free to skip the implementation if they so desire:

```kotlin
class FooRevisedImpl : FooRevised()
{
    // No need to override barExtension(), it compiles just fine!
}
```
