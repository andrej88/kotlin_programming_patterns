# Super Enums

Enum classes in Java and Kotlin are quite powerful, but sometimes seem redundant next to Kotlin's Sealed classes and objects.

An enum class can easily be rewritten as sealed class:

```kotlin
enum class State
{
    IDLE, WALK, RUN, CRAWL
}
```

becomes

```kotlin
sealed class State
{
    object IDLE : State()
    object WALK : State()
    object RUN : State()
    object CRAWL : State()
}
```

The IntelliJ Kotlin plugin even offers an intention to perform this conversion.

The enum version is more readable, but the sealed class version is more powerful in a number of ways.

## Hierarchical Enum

We can expand the basic sealed class example to allow for nested sealed classes.

```kotlin
sealed class State
{
    object IDLE : State()

    sealed class Moving : State()
    {
        object WALK : Moving()
        object RUN : Moving()
        object CRAWL : Moving()
    }
}

```

This gives us a lot of flexibility. For example, we can declare and define properties exclusive to only certain classes of values. Even without adding anything else to the classes, this structure can simplify our `when` blocks in some circumstances:

```kotlin
when (state)
{
    State.IDLE -> // ...
    is State.Moving -> // ...
}
```

Or if we wish to check against every value:

```kotlin
when (state)
{
    State.IDLE -> 1
    State.Moving.WALK -> 2
    State.Moving.RUN -> 2
    State.Moving.CRAWL -> 2
}
```

Both of these `when` statements are recognized as exhaustive, and can therefore be used as expressions (e.g. `return when(state) { ... }`).

```kotlin
sealed class State
{

    abstract fun onEnter(previousState: State)

    object Idle : State()
    {
        override fun onEnter(previousState: State)
        {

        }
    }
}

```
